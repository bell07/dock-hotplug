# dock-hotplug 

## Script to handle external display hotplug for Linux handheld devices.
Compatible to Linux-driven Nintendo Switch with official docking station or third-party HDMI adapters.

The behaviour can be set in /etc/dock-hotplug.env
IF undocked / no external display connected, only HANDHELD mode does sense ;-).

```
## Supported docked video modes
 - TV only (default)
 - HANDHELD only
 - MIRROR - both the same. TV is scaled from 1280x720
 - TVLEFT
 - TVRIGHT

## Supported docked audio modes
 - HDMI profile only (default)
 - HANDHELD profile only
 - BOTH keep both profiles enabled

## Supported devices (See definition in /usr/share/dock-hotplug/devices/)
# - switch  -     Nintendo Switch (default)
# - gpdwin2 -     GPD Win 2
# - modesetting - Default x86/amd64 device with intel modesetting driver
# - intel       - Default x86/amd64 device with old intel driver

DEVICE="switch"
```

The parameter DEVICE selects a file in /usr/share/dock-hotplug/${DEVICE}.
The file is sourced by bash and contains device related settings.
Look into the existing files, if you need to setup new not supported device.
